<div id="footer-wrap">
  <footer>
    <div id="footer-connect">
      <p>Connect with me</p>
      <ul>
        <li>
          <a class="stackoverflow" href="http://stackoverflow.com/users/884521/muhammed-athimannil" target="_blank">
            <img src="./img/stack.svg" alt="stackoverflow">
          </a>  
        </li>
        <li>
          <a class="linkedin" href="http://uk.linkedin.com/in/msathimannil/" target="_blank">
            <img src="./img/linkedin.svg" alt="linkedin">
          </a>
        </li>
        <li>
          <a class="twitter" href="https://twitter.com/MSAthimannil" target="_blank">
            <img src="./img/twitter.svg" alt="twitter">
          </a>
        </li>
        <li>
          <a class="facebook" href="https://www.facebook.com/MSAthimannil" target="_blank">
            <img src="./img/facebook.svg" alt="facebook">
          </a>
        </li>
      </ul>
    </div>
    <ul class="used-skills">
      <li><img src="./img/html5.svg" alt="html5"></li>
      <li><img src="./img/css3.svg" alt="CSS3"></li>
      <li><img src="./img/js.svg" alt="javascript"></li>
      <li><img src="./img/sass.svg" alt="Sass"></li>
      <li><img src="./img/gulp.svg" alt="gulp"></li>
      <li><img src="./img/php.svg" alt="PHP"></li>
    </ul>
    <!-- <p id="copy-right">&copy; 2014 Muhammed Athimannil  - All right reserved</p> -->
    <p id="copy-right">&copy; <?php echo date('Y') ?> Muhammed Athimannil  - All right reserved</p>
  </footer>
</div>