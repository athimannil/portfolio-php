function notifyResult(name, alertMsg) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification("Hi " + name,{
        lang: "en",
        body: alertMsg,
        icon: "./img/muhammed.jpg",
        // tag: "notify_1"
    });
  }


  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Hi " + name,{
            lang: "en",
            body: alertMsg,
            icon: "./img/muhammed.jpg",
            // tag: "notify_1"
        });
      }
    });
  }

  // At last, if the user has denied notifications, and you 
  // want to be respectful there is no need to bother them any more.
}

function submitForm () {
  var name = document.getElementById("name"),
      email = document.getElementById("email"),
      comment = document.getElementById("comment");
  var data = JSON.stringify({name: name.value, email: email.value, comment: comment.value});
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "sendmail.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(data);
  xhttp.onreadystatechange = function(){
      if (xhttp.readyState == 4 && xhttp.status == 200){
        var mailResult = JSON.parse(xhttp.response);
        if (mailResult.success){
          notifyResult(name.value, mailResult.result);
          name.value = "";
          email.value = "";
          comment.value = "";
        }
      }
  };
}