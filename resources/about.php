<!doctype html>
<html lang="en">
<head>
	<meta property="fb:app_id" 		content="1492692317" />
	<meta property="og:type"  		content="website" />
	<meta property="og:url"    		content="http://www.athimannil.com/" />
	<meta property="og:title"  		content="Muhammed Athimannil - Front End Developer" />
	<meta property="og:description" content="I also have a keen interest in responsive web development and have recently started putting this into practice." />
	<meta property="og:image"  		content="http://www.athimannil.com/img/muhammed.jpg" />
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Muhammed Athimannil | Frontend Developer based in Berlin | About me</title>
	<link rel="stylesheet" href="css/app.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<header>
		<a id="logo" href="index.php"><h2>athimannil</h2></a>
		<input id="checkbox" type="checkbox">
		<label for="checkbox" class="menu-icon">
			<span class="menu-line"></span>
			<span class="menu-line"></span>
			<span class="menu-line"></span>
		</label>
		<nav>
			<a href="index.php">HOME</a>
			<a class="active" href="about.php">ABOUT ME</a>
			<a href="portfolio.php">PORTFOLIO</a>
			<a href="contact.php">CONTACT</a>
			<a href="cv.php">CV</a>
		</nav>
	</header>
	<section class="about-me">
		<h1 class="title">About me</h1>
		<article class="description who-iam">
		  <h3>Who I am</h3>
		  <p>I would like to introduce my self as a web developer. Since 2008, I have been working in web industry. I am passionate about producing creative, unique visuals that clearly communicate a brand’s message. Particularly those related to the arts or those that engage young people. I then transform them into clean, standards-based markup.</p>
		  <p>I also have a keen interest in mobile / responsive web development and have recently started putting this into practice, as you can see by this very website. I work in front-end and back-end development with UI/UX designers by having them outsource work to me and also work with my own, direct clients as you can see by most of the pieces in my portfolio. I am happy to work either remotely or in-house, whichever works best.</p>
		</article>
		<article class="illustration myself">
		  <figure class="thumbnail">
			<img src="img/muhammed.jpg" alt="Muhammed Athimannil">
			<figcaption>Muhammed Athimannil</figcaption>
		  </figure>
		</article>
		<article class="description what-ilove">
		  <h3>What I love</h3>
		  <p>I love to create unique clear responsive sites that are user friendly. All my works comply with web standards, elegant with hand coded. I strive to write code of the highest standard and bug fix in an efficient and extensible way.</p>
		  <p>I work for both front-end and back-end websites for over 4 years. some of them I built using WordPress CMS, which is the most used content management system in the world. Every website is tailored to client needs and created from scratch without using any templates.</p>
		  <p>I developed websites for bigger companies as part of a team, if you need examples of those websites - drop me a line</p>
		</article>
		<article class="illustration myskills">
		  <h3>Skills</h3>
		  <ul class="skills">
			<li style="width:90%;">HTML5</li>
			<li style="width:92%;">CSS3</li>
			<li style="width:69%;">JavaScript</li>
			<li style="width:81%;">jQuery</li>
			<li style="width:66%;">Sass / Less</li>
			<li style="width:85%;">Bootstrap</li>
			<li style="width:62%;">PHP</li>
			<li style="width:56%;">MySQL</li>
		  </ul>
		</article>
	</section>
	<aside>
	  <h3>Social media</h3>
	  <ul class="social-media">
		<li>
		  <a class="twitter" href="https://twitter.com/MSAthimannil" target="_blank">
			<span class="social-image"></span>
			<div class="hover-move">
			  <span class="label">Twitter</span>
			  <span class="label">MSAthimannil</span>
			</div>
		  </a>
		</li>
		<li>
		  <a class="facebook" href="https://www.facebook.com/MSAthimannil" target="_blank">
		  <span class="social-image"></span>Facebook</a>
		</li>
		<li>
		  <a class="gplus" href="https://plus.google.com/101426681494840024341/posts" target="_blank">
		  <span class="social-image"></span>Google plus</a>
		</li>
		<li>
		  <a class="linkedin" href="http://uk.linkedin.com/in/msathimannil/" target="_blank">
		  <span class="social-image"></span>LinkedIn</a>
		</li>
		<li>
		  <a class="skype" href="skype:athimannil?call">
			  <span class="social-image"></span>
			  <div class="hover-move">
				<span class="label">Skype</span>
				<span class="label">athimannil</span>
			  </div>
		  </a>
		</li>
		<li>
		  <a class="instagram" href="http://instagram.com/msathimannil" target="_blank">
			<span class="social-image"></span>
			  <div class="hover-move">
				<span class="label">Instagram</span>
				<span class="label">msathimannil</span>
			  </div>
		  </a>
		</li>
	  </ul>
	</aside>
	<?php require 'include/footer.php'; ?>
</body>
</html>
