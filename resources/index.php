<!doctype html>
<html lang="en">
<head>
	<meta property="fb:app_id" 		content="1492692317" />
	<meta property="og:type"  		content="website" />
	<meta property="og:url"    		content="http://www.athimannil.com/" />
	<meta property="og:title"  		content="Muhammed Athimannil - Front End Developer" />
	<meta property="og:description" content="I also have a keen interest in responsive web development and have recently started putting this into practice." />
	<meta property="og:image"  		content="http://www.athimannil.com/img/muhammed.jpg" /> 
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Muhammed Athimannil | Frontend Developer based in Berlin</title>
	<link rel="stylesheet" href="css/app.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<header>
		<a id="logo" href="index.php"><h2>athimannil</h2></a>
		<input id="checkbox" type="checkbox">
		<label for="checkbox" class="menu-icon">
			<span class="menu-line"></span>
			<span class="menu-line"></span>
			<span class="menu-line"></span>
		</label>
		<nav>
			<a class="active" href="index.php">HOME</a>
			<a href="about.php">ABOUT ME</a>
			<a href="portfolio.php">PORTFOLIO</a>
			<a href="contact.php">CONTACT</a>
			<a href="cv.php">CV</a>
		</nav>
	</header>

<div id="intro-wrap">
  <section id="introduction">
	<h1>Hello, I'm <span id="name">Muhammed Athimannil</span> and I am a creative freelance web developer based in <span id="city">Berlin</span><span id="country">UK</span></h1>
	<blockquote>
	  <p id="quote">Every nice creations start from imagination followed by dedication</p>
	</blockquote>
  </section>
</div>

<section class="services">
  <article>
	<img src="./img/screens.svg" alt="screens">
	<h2>WHO I AM</h2>
	<p>I also have a keen interest in responsive web development and have recently started putting this into practice, as you can see by this very website.</p>
	<a class="link-btn" href="/about.php">Read more</a>
  </article>
  <article>
	<img src="./img/cogs.svg" alt="Cogs">
	<h2>WHAT I DO</h2>
	<p>I create unique, clean sites that are easy to navigate. All my works comply with web standards, use the latest industry techniques especially HTM5 and CSS3.</p>
	<a class="link-btn" href="/portfolio.php">Read more</a>
  </article>
  <article>
	<img src="./img/cups.svg" alt="cups">
	<h2>WHAT I LOVE</h2>
	<p>I love to create unique clear responsive sites that are user friendly. All of my works comply with web standards and elegant with hand coded.</p>
	<a class="link-btn" href="/about.php">Read more</a>
  </article>
</section>

<section class="clients">
	<h2>Teams I worked with</h2>
	<img class="bt" src="img/bt.svg" alt="British Telecom">
	<img src="img/microsoft.svg" alt="Microsoft">
	<img src="img/ucl.svg" alt="University College of London">
	<img src="img/ba.svg" alt="British Airways">
	<img src="img/john-lewis.svg" alt="John Lewis">
	<img src="img/marriot.svg" alt="Marriot">
	<img src="img/ui-centric.svg" alt="UI Centric">
</section>

<?php require 'include/footer.php'; ?>
</body>
</html>