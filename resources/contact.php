<!doctype html>
<html lang="en">
<head>
  <meta property="fb:app_id"    content="1492692317" />
  <meta property="og:type"      content="website" />
  <meta property="og:url"       content="http://www.athimannil.com/" />
  <meta property="og:title"     content="Muhammed Athimannil - Front End Developer" />
  <meta property="og:description" content="I also have a keen interest in responsive web development and have recently started putting this into practice." />
  <meta property="og:image"     content="http://www.athimannil.com/img/muhammed.jpg" />
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Muhammed Athimannil | Frontend Developer based in Berlin | Contact</title>
	<link rel="stylesheet" href="css/app.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

    <header>
        <a id="logo" href="index.php"><h2>athimannil</h2></a>
        <input id="checkbox" type="checkbox">
        <label for="checkbox" class="menu-icon">
            <span class="menu-line"></span>
            <span class="menu-line"></span>
            <span class="menu-line"></span>
        </label>
        <nav>
            <a href="index.php">HOME</a>
            <a href="about.php">ABOUT ME</a>
            <a href="portfolio.php">PORTFOLIO</a>
            <a class="active" href="contact.php">CONTACT</a>
            <a href="cv.php">CV</a>
        </nav>
    </header>

  <div id="my-map"></div>

  <section>
    <ul class="contact-info">
      <li>
        <a class="contact-me" href="mailto:hello@athimannil.com">
          <div class="contact-icon">
            <svg class="envelop" width="55" height="33" viewBox="0 0 55 33" xmlns="http://www.w3.org/2000/svg">
              <g fill="#FFF" fill-rule="evenodd">
                <path class="open" d="M26.38 21.08L1.38.1h50l-25 20.98z" />
                <path d="M17.52 16.534L0 31.237V1.83l17.52 14.704zM19.792 18.44l7.688 6.45 7.687-6.45 17.33 14.542H2.463L19.792 18.44zM37.438 16.534L54.958 1.83v29.407L37.44 16.534z" />
              </g>
            </svg>
          </div>
          <span>hello@athimannil.com</span>
        </a>
      </li>
      <li>
        <a class="contact-me" href="tel:+4915782119447">
          <div class="contact-icon">
            <svg class="mobile" width="32" height="60" viewBox="0 0 32 60" xmlns="http://www.w3.org/2000/svg">
              <path d="M0 2.472C0 1.24 1.207 0 2.438 0h27.15C30.792 0 32 1.223 32 2.472v55.093C32 58.755 30.837 60 29.59 60c-1.25 0-25.914-.01-27.152 0C1.2 60.01 0 58.8 0 57.565V2.472zM16 57.5c1.36 0 2.462-1.12 2.462-2.5 0-1.382-1.103-2.5-2.462-2.5-1.36 0-2.462 1.118-2.462 2.5 0 1.38 1.103 2.5 2.462 2.5zM2.462 6.24h27.076V50H2.462V6.24z"
              fill="#FFF" fill-rule="evenodd" />
            </svg>
          </div>
          <span>+49 (0)157 8211 9447</span>
        </a>
      </li>
    </ul>
    <div id="contact-form">
      <h1 id="contact">Contact</h1>
      <p>Take a look around the site, Do you have something in mind you would like to let me know .You can contact me by filling the form below or email hello@athimannil.com</p>
      <form onsubmit="submitForm(); return false;">
        <div class="form-wrap">
          <input type="text" name="name" id="name" placeholder="Name" required>
          <label for="name">Name</label>
        </div>
        <div class="form-wrap">
          <input type="email" name="email" id="email" placeholder="Email" required>
          <label for="email">Email</label>
        </div>
        <div class="form-wrap">
          <textarea name="comment" id="comment" placeholder="Comment" cols="30" rows="10" required></textarea>
          <label for="comment">Message</label>
        </div>
        <input type="submit" name="submit" value="Send">
      </form>
    </div>
  </section>

  <?php require 'include/footer.php'; ?>
  <script>
    function loadScript() {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDtwLGMk2mRH8pFkhJrRtJ0lTyT0PokK4Q&callback=initialize";
        document.body.appendChild(script);
    }
    window.onload = loadScript;
  </script>
  <script src="js/core.js"></script>
</body>
</html>
