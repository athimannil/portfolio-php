<!doctype html>
<html lang="en">
<head>
	<meta property="fb:app_id" 		content="1492692317" />
	<meta property="og:type"  		content="website" />
	<meta property="og:url"    		content="http://www.athimannil.com/" />
	<meta property="og:title"  		content="Muhammed Athimannil - Front End Developer" />
	<meta property="og:description" content="I also have a keen interest in responsive web development and have recently started putting this into practice." />
	<meta property="og:image"  		content="http://www.athimannil.com/img/muhammed.jpg" />
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Muhammed Athimannil | Frontend Developer based in Berlin | Portfolio</title>
	<link rel="stylesheet" href="css/app.css">
	<link rel="stylesheet" href="css/player.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<header>
		<a id="logo" href="index.php"><h2>athimannil</h2></a>
		<input id="checkbox" type="checkbox">
		<label for="checkbox" class="menu-icon">
			<span class="menu-line"></span>
			<span class="menu-line"></span>
			<span class="menu-line"></span>
		</label>
		<nav>
			<a href="index.php">HOME</a>
			<a href="about.php">ABOUT ME</a>
			<a class="active" href="portfolio.php">PORTFOLIO</a>
			<a href="contact.php">CONTACT</a>
			<a href="cv.php">CV</a>
		</nav>
	</header>
	<section class="gallery">
		<article id="skinn" ng-init="player()">
			<video id="kuttuplayer" poster="./img/screenshot/tv.jpg">
			  <source src="video/machine-learning.webm" type="video/webm">
			  <source src="video/machine-learning.ogv" type="video/ogv">
			  <source src="video/machine-learning.mp4" type="video/mp4">
			  <p>
			    Your browser doesn't support HTML5 video.
			    <a href="video/machine-learning.mp4">Download</a> the video instead.
			  </p>
			</video>
			<div id="controller">
			  <div class="icon play" id="playpause"></div>
			  <div class="time" id="playing-time">00:00</div>
			  <div id="seekbar">
			    <div id="played"></div>
			    <div id="loaded"></div>
			  </div>
			  <div class="time" id="total-time">00:00</div>
			  <div id="volume">
			    <div id="speaker" class="icon speaker volume-max"><span class="icon"></span></div>
			    <div id="voumecontrol">
			      <div id="volumeposition">
			        <div id="volumenow"></div>
			      </div>
			    </div>
			  </div>
			  <div id="fullscreen" class="icon tofullscreen"><span class="icon"></span></div>
			</div>
		</article>
		<figure>
			<a href="#" target="_blank">
				<img src="./img/screenshot/surface.png" alt="Microsoft Surface">
			</a>
		</figure>
		<figure>
			<img src="./img/screenshot/foznol.png" alt="Foznol">
		</figure>
		<figure>
			<img src="./img/screenshot/holiday-inn.png" alt="Holiday Inn">
		</figure>
		<figure>
			<a href="http://extend.ucl.ac.uk/" target="_blank">
		      <img src="./img/screenshot/ucl.png" alt="University College of London">
		    </a>
		</figure>
		<figure>
			<a href="http://www.gomolearning.com/" target="_blank">
				<img src="./img/screenshot/ba.png" alt="British Airways">
			</a>
		</figure>
		<figure>
			<a href="http://www.lovespace.co.uk/" target="_blank">
				<img src="./img/screenshot/love-space.png" alt="Love Space">
			</a>
		</figure>
		<figure>
			<img src="./img/screenshot/resolor.png" alt="Resolor">
		</figure>
		<figure>
			<a href="http://www.design-portfolio.co.uk/" target="_blank">
				<img src="./img/screenshot/design-portfolio.png" alt="Design Portfolio">
			</a>
		</figure>
		<hr>
		<figure>
			<a href="http://business.bt.com/" target="_blank">
				<img src="./img/screenshot/bt.jpg" alt="British Telecom">
			</a>
			<figcaption>
				<span>HTML5 </span>
				<span>CSS3 </span>
				<span>Sass </span>
				<span>JavaScript </span>
				<span>JSON </span>
			</figcaption>
		</figure>
		<figure>
			<a href="http://www.digimobgroup.com/" target="_blank">
				<img src="./img/screenshot/digimob.jpg" alt="Digimob Group">
			</a>
			<figcaption>
				<span>HTML5 </span>
        		<span>CSS3 </span>
        		<span>Sass </span>
        		<span>JavaScript </span>
        		<span>JSON </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot/hub.jpg" alt="BT Web App">
			<figcaption>
				<span>HTML5 </span>
				<span>CSS3 </span>
				<span>javaScript </span>
				<span>jQuery </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot/digimobjobs.jpg" alt="Digimob Jobs">
			<figcaption>
				<span>HTML5 </span>
				<span>CSS3 </span>
				<span>jQuery </span>
				<span>PHP5 </span>
				<span>MySQL </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot/flatui-admin.jpg" alt="digimob admin panel">
			<figcaption>
				<span>Bootstrap </span>
				<span>Sass </span>
				<span>CSS3 </span>
				<span>PHP5 </span>
				<span>MySQL </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot/smart-puff.jpg" alt="Smart Puff">
			<figcaption>
				<span>WordPress </span>
				<span>HTML5 </span>
				<span>CSS3 </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot/admin.jpg" alt="Love City">
			<figcaption>
				<span>HTML5 </span>
				<span>CSS3 </span>
				<span>OOPHP </span>
				<span>MySQL </span>
				<span>JSON </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot//woodgreen-films.jpg" alt="Woodgreen Films">
			<figcaption>
				<span>HTML5 </span>
				<span>CSS3 </span>
				<span>jQuery </span>
				<span>PHP5 </span>
				<span>MySQL </span>
			</figcaption>
		</figure>
		<figure>
			<img src="./img/screenshot/lovecity.jpg" alt="Love City">
			<figcaption>
				<span>HTML5 </span>
				<span>CSS3 </span>
				<span>jQuery </span>
				<span>PHP5 </span>
				<span>MySQL </span>
				<span>AJAX </span>
			</figcaption>
		</figure>
	</section>
	<?php require 'include/footer.php'; ?>
<script src="js/core.js"></script>
</body>
</html>
