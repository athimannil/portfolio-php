<?php
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$name = $request->name;
$email = $request->email;
$comment = $request->comment;

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data
// validate the variables
if (empty($name)){
  $errors['name'] = 'Name is required.';
} 
if (empty($email)){
  $errors['email'] = 'Email is required.';
}
if (empty($comment)){
  $errors['comment'] = 'Message is required.';
}

if (!empty($errors)){
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['result'] = 'Please check the fields in red';
}else{
    // if there are no errors, return a message
    $data['success'] = true;
    $data['result'] = 'Thank you for contacting with me. I will respond as soon as possible.';
    // CHANGE THE TWO LINES BELOW
    $email_to = "hello@athimannil.com";
    $email_subject = "From portfolio";

    $email_message = "Name: ".$name."\n";
    $email_message .= "Email: ".$email."\n\n";
    $email_message .= "Message: ".$comment."\n\n";

    $headers = 'From: '.$email."\r\n".
    'Reply-To: '.$email."\r\n" .
    'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject, $email_message, $headers); 
}
echo json_encode($data);


// Include the Autoloader (see "Libraries" for install instructions)
/* Mailgun
require 'vendor/autoload.php';
use Mailgun\Mailgun;
// Instantiate the client.
$mgClient = new Mailgun('key-deddab69d169a925c881aa372bf7d67e');
// $domain = "samples.mailgun.org";
// $domain = "sandboxd88f4b16415c4ca483ce2c4da09097a1.mailgun.org";
$domain = "athimannil.com";
// Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'to'      => 'Muhammed Athimannil <athimannil@hotmail.com>',
    'from'    => $name .'<'.$email.'>',
    'subject' => 'From portfolio',
    'text'    => $comment
));
echo json_encode($result);
*/
?>