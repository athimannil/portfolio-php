<!doctype html>
<html lang="en">
<head>
	<meta property="fb:app_id" 		content="1492692317" />
	<meta property="og:type"  		content="website" />
	<meta property="og:url"    		content="http://www.athimannil.com/" />
	<meta property="og:title"  		content="Muhammed Athimannil - Front End Developer" />
	<meta property="og:description" content="I also have a keen interest in responsive web development and have recently started putting this into practice." />
	<meta property="og:image"  		content="http://www.athimannil.com/img/muhammed.jpg" />
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Muhammed Athimannil | Frontend Developer based in Berlin | CV</title>
	<link rel="stylesheet" href="css/app.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<header>
		<a id="logo" href="index.php"><h2>athimannil</h2></a>
		<input id="checkbox" type="checkbox">
		<label for="checkbox" class="menu-icon">
			<span class="menu-line"></span>
			<span class="menu-line"></span>
			<span class="menu-line"></span>
		</label>
		<nav>
			<a href="index.php">HOME</a>
			<a href="about.php">ABOUT ME</a>
			<a href="portfolio.php">PORTFOLIO</a>
			<a href="contact.php">CONTACT</a>
			<a class="active" href="cv.php">CV</a>
		</nav>
	</header>

	<section class="cv-personal">
		<figure class="thumbnail">
			<img src="img/muhammed.jpg" alt="Muhammed Athimannil">
			<figcaption>Muhammed Athimannil</figcaption>
		</figure>
		<h1>Muhammed Athimannil</h1>
		<h3>Frontend Developer</h3>
		<hr>
		<address>Berlin<br>Germany</address>
		<p>m: <a href="tel:+4915782119447">+49 (0)157 8211 9447</a></p>
		<p>e: <a href="mailto:hello@athimannil.com">hello@athimannil.com</a></p>
	</section>

	<section class="cv-title"><h2>Personal profile</h2></section>
	<section class="summary">
	  <p>I am a technical enthusiast having outstanding skills and passion for Web Develop- ment. I find it really thrilling to collaborate designing &amp; programming and to be a web developer. I have experience in developing Front-End and Back-End, with the goal of delivering cutting edge user interface.</p>
	  <p>I am fond of implementing modern emerging technologies such as HTML5, CSS3, Responsive Design and different kinds of framework and integrating them together to keep cross browser compatibility graceful in degrades browsers. I strive to write codes of the highest standard and bug fix in an efficient and extensible way. I would like to have bright career in the same field where I can contribute my skills to the maximum.</p>
	</section>

	<section class="cv-title"><h2>Experience</h2></section>
	<section class="timeline">
		<div class="position">
			<div class="date">Jan 2017 - Present</div>
			<a class="circle active"></a>
			<div class="position-description">
				<h2 class="position-title ">Frontend Engineer</h2>
				<h4 class="company">Caterwings</h4>
				<h5 class="location">Berlin, Germany</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Maintaining current company website</li>
						<li>Build new multi-language website from scratch</li>
						<li>Ajax and RESTFul Services</li>
						<li>Google Maps API, YouTube API..etc</li>
						<li>Bootstrap 3 & Bootstrap 4</li>
						<li>CSS Flexbox</li>
						<li>Responsive Design</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date">Jun 2016 - Dec 2016</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Frontend Developer</h2>
				<h4 class="company">Carspring</h4>
				<h5 class="location">Berlin, Germany</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Participate in design and code review</li>
						<li>Ajax and RESTFul Services</li>
						<li>Responsive web design applications</li>
						<li>Bootstrap</li>
						<li>Web App</li>
						<li>AngularJS</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date">Aug 2015 - Jun 2016</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Front End Software Engineer</h2>
				<h4 class="company">brndstr</h4>
				<h5 class="location">Dubai, United Arab Emirates</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Bootstrap</li>
						<li>Web App</li>
						<li>AngularJS</li>
						<li>ruby on rails</li>
						<li>haml</li>
						<li>SVG</li>
						<li>snap.js</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date">Jan 2015 -  Aug 2015</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title">UI Developer</h2>
				<h4 class="company">Psybo Technologies</h4>
				<h5 class="location">Keralam, India</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>HTML5</li>
						<li>CSS3</li>
						<li>Bootstrap</li>
						<li>Responsive Design</li>
						<li>AngularJS</li>
						<li>Web App</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Nov 2013 -  Dec 2014</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title">Developer</h2>
				<h4 class="company">LEO</h4>
				<h5 class="location">Brighton, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Web App (gomolearning.com)</li>
						<li>Moodle</li>
						<li>Bootstrap</li>
						<li>Responsive Design</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Sep 2013 -  Oct 2013</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Front End Developer</h2>
				<h4 class="company ">Morph London</h4>
				<h5 class="location ">London, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<p>Build responsive WordPress theme in Bootstrap 3 for lovespace.co.uk</p>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Aug 2013 -  Sep 2013</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Web Developer</h2>
				<h4 class="company ">Design Portfolio</h4>
				<h5 class="location ">London, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Brand new website</li>
						<li>Responsive Web Design</li>
						<li>Parallax scrolling</li>
						<li>Twitter bootstrap</li>
						<li>Google Maps API</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Jul 2013 - Aug 2013</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Web Developer</h2>
				<h4 class="company ">Digimobjobs</h4>
				<h5 class="location ">London, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Brand new parallax scrolling</li>
						<li>Responsive Web Design</li>
						<li>PHP (CodeIgniter)</li>
						<li>API</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Apr 2013 - Jun 2013</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Web Developer</h2>
				<h4 class="company ">Nitrogen, Huntsworth Health</h4>
				<h5 class="location ">London, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Building HTML5 iOS web applications.</li>
						<li>Awesome animations made with HTML5 and CSS3 </li>
						<li>Animated graphs and chart </li>
						<li>Responsive design including retina display </li>
						<li>Twitter bootstrap </li>
						<li>Cross browser compatibility</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Sep 2009 - Mar 2013</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Front End Developer</h2>
				<h4 class="company ">UI Centric</h4>
				<h5 class="location ">London, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<p>Primarily working on BT business project with UX designers. Frequently keep bug fix with Back-End people . I gave HTML5, CSS3 &amp; JavaScript support on application for Microsoft too.</p>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Fresh projects with hand code</li>
						<li>Polishing existing version </li>
						<li>Bundles of JavaScript functions</li>
						<li>Debugging JavaScript and jQuery</li>
						<li>Cross browser compatibility</li>
						<li>Version control (SVN and Git)</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Apr 2011 - Sep 2011</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">Web Developer</h2>
				<h4 class="company ">Digimobjobs</h4>
				<h5 class="location ">London, United Kingdom</h5>
				<div class="responsibilities">
					<hr>
					<p>The website was really outdated by not using any recent advancements in the web developing. I could completely redesign it with latest state-of-the-art technologies. I was responsible for a variety of tasks including Front-End, Back-End, User Experience,
						Server setup and maintenance.</p>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Convert PSD to HTML5,/CSS3 with jQuery and JavaScript.</li>
						<li>Hand coded HTML5, CSS3, PHP..etc with TextMate</li>
						<li>API integration (Linkedin, Google Maps, Youtube, facebook, Twitter and bit.ly).</li>
						<li>CMS via OO PHP &amp; MySQL</li>
						<li>Implemented Google Analytics</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="cv-title"><h2>Education</h2></section>
	<section class="timeline">
		<div class="position">
			<div class="date ">Sep 2009 - Sep 2010</div>
			<a class="circle active"></a>
			<div class="position-description">
				<h2 class="position-title ">Graduation Certificate in Computing</h2>
				<h4 class="company ">London Metropolitan University</h4>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Creative Industries</li>
						<li>Business Solutions</li>
						<li>Web Applications Design</li>
						<li>Research and Development Skills</li>
						<li>Digital Solutions Issues</li>
						<li>Emerging Web Technology and Applications</li>
						<li>Development Project</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="position">
			<div class="date ">Jan 2009 - Aug 2009</div>
			<a class="circle"></a>
			<div class="position-description">
				<h2 class="position-title ">MSc Internet Applications Development</h2>
				<h4 class="company ">London Metropolitan University</h4>
				<div class="responsibilities">
					<hr>
					<ul style="padding-left: 20px; list-style-type: disc; margin-bottom:15px;">
						<li>Essential Computer Science</li>
						<li>Object Oriented Programming</li>
						<li>Database Design and Implementation</li>
						<li>Internet Applications Development</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="cv-title"><h2>Testimonials</h2></section>
	<section>
		<blockquote class="testimonial">
			<p>Muhammed was a great contribution to Carspring team. He is a hardworking and details-oriented front-end developer, willing to share his knowledge with and help to the rest of the team. In addition to his understanding of technicalities behind each development work, he tries to understand the business logic and value behind the features, which in return contributed to his good quality work in Carspring. I would highly recommend Muhammed to any company looking for reliable front-end developer.</p>
			<small>
				<a href="#">Büsra Cekicler</a>
				<cite> | Product Manager at Carspring</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammed is an excellent, self-organised frontend developer. He has a strong analytical understanding of complex user interfaces and a highly appreciated focus on the customer journey. It was a pleasure working with him.</p>
			<small>
				<a href="#">Oliver Kriz</a>
				<cite> | Lead Developer Engineering at Carspring</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammed was a great contribution to the success of Carspring. He is an excellent frontend developer always delivering his tasks with high quality and on time. He understands new features and/or improvements quickly and works standalone. In addition he also tries to improve the work by understanding the business value of his development tasks. I would highly recommend Muhammed to any company looking to renew/improve their frontend (mobile and/or desktop).</p>
			<small>
				<a href="#">Dr. Peter Baumgart</a>
				<cite> | Co-Founder / Managing Director at Carspring</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>I had the chance to work with Muhammed at Carspring. He is a hardworking & details-oriented Frontend Developer. According to my experience with him , he is a fast learner. He shares his knowledge with his colleagues and willingly help others. Moreover, I assert that he is a good & polite person. Therefore, I strongly recommend him.</p>
			<small>
				<a href="#">Achraf Sallemi</a>
				<cite> | Backend Developer at Carspring</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammed is a great FE developer and even as a person.</p>
			<p>He is a top-developer, who is always following newest FE technologies, as well working with them and implementing them.</p>
			<p>I highly recommend him for companies, who are searching for trustable dev who will not fail his tasks.</p>
			<small>
				<a href="#">Janis Bizkovskis</a>
				<cite> | Software Engineer at Carspring</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>"I highly recommend Mr. Muhammed" He is detailed oriented and produced great results for the Psybo Technologies. He has a great experience in HTML5, CSS3, Cross Browser Compatibility, Responsive Web Structure, UI/UX Development.</p>
			<small>
				<a href="#">Lais Chalithodi</a>
				<cite> | CEO at Psybo Technologies</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>I worked with Muhammed on a number of projects at LEO. Muhammed is a talented front end developer with a keen eye for what looks good and in depth knowledge of and skill with CSS. I was impressed by his ability to quickly spot solutions to CSS related problems and learnt a couple of CSS tricks from him.</p>
			<p>Due to available resources and the demands of the company at the time, Muhammed was placed on many back end PHP (Moodle) projects requiring him to quickly get up to speed with Moodle's particular structure and quirks. Despite the mismatch between the work and Muhammed's primary skill set, he worked hard, remained positive, asked for help when he needed it and produced the required outcome.</p>
			<small>
				<a href="#">Andrew Downes</a>
				<cite> | Solutions Architect at LEO Learning</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammed was a developer within the Platforms team, where he was one of the Moodle implementers. Among other projects, he worked on an integration project, communicating between an open source Learning Management System (Moodle) and an open source Learning Record Store.</p>
			<p>I found Muhammed to be a conscientious and hard worker, who is eager to learn from others and take on new technologies and methodologies, He was always happy to go the extra mile if needed and assist or guide other developers where applicable.﻿</p>
			<small>
				<a href="#">Ferry van der Vorst</a>
				<cite> | Business Unit Director at LEO Learning</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammad was reliable, trustworthy and delivered on every aspect of the project as required.</p>
			<small>
				<a href="#">Alison Carey </a>
				<cite> | Director at Design Portfolio</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>We hired Muhammed as a freelance web developer to work on a specific project. He helped us to achieve what we were after with great result. I was very impressed by his dedication to the task at hand, going above and beyond the typical commitment you might receive from a freelancer. Muhammed was a pleasure to work with and his input throughout the project was invaluable.</p>
			<small>
				<a href="#">Lesley Perez  </a>
				<cite> | Creative Director at Design Portfolio</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>I worked with Muhammed on the British Telecom project, a service portal for business customers. He helped with scoping and delivery of HTML/CSS. We were pressed for time and in-spite of his Ramadan fasting he helped with project delivery sticking to guidelines and suggesting areas for improvement. Qualities: Collaborative, Patient listener, and Gets work done. I would definitely love to work with him on some of my personal projects that need work in the coming months.</p>
			<small><a href="#">sreeramen ramaswamy santhanam </a> <cite> | Information Architect at UI Centric</cite></small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammed is very hard working individual. He was ammenable to changes and getting our website absolutely right. I cannot recommend him highly enough..</p>
			<small>
				<a href="#">Daniel Goldstein </a>
				<cite> | Managing Director at Digimob</cite>
			</small>
		</blockquote>
		<blockquote class="testimonial">
			<p>Muhammed is a polite and energetic student. He is pleasant to work with. I have taught him web design and e-commerce modules. He completed all his assignments on time and added a creative touch when appropriate. I would recommend him to any prospective company that needs his skills.</p>
			<small>
				<a href="#">Yanguo Jing </a>
				<cite> | Associate Professor in Computer Science at London Metropolitan University</cite>
			</small>
		</blockquote>
	</section>

	<?php require 'include/footer.php'; ?>
	<script src="//code.jquery.com/jquery-latest.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.circle').click(function(){
				$(this).toggleClass('active');
				$(this).next().children('.responsibilities').slideToggle();
			});
		});
	</script>
</body>
</html>
