var gulp         = require('gulp');
var sass         = require('gulp-sass');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker     = require('css-mqpacker');
var cssnano      = require('cssnano');
var concat       = require('gulp-concat');
var connect      = require('gulp-connect');
var notify       = require("gulp-notify");
var connect      = require('gulp-connect-php');
var livereload   = require('gulp-livereload');


var config = {
	sassDir : './resources/sass',
	jsPath  : './resources/javascript',
	htmlPath: './resources',
	fontDir : './resources/fonts',
	imageDir: './resources/images',
	videoDir: './resources/videos',
};

var processors = [
	autoprefixer({browsers: 'last 2 versions, ie 10, iOS 8, Android 4'}),
	mqpacker,
	cssnano,
];

gulp.task('files', function() {
	return gulp.src(config.htmlPath +'/**.php')
		.pipe(gulp.dest('./public'))
		.pipe(livereload());
});
gulp.task('footer', function() {
	return gulp.src(config.htmlPath +'/include/**.php')
		.pipe(gulp.dest('./public/include'))
		.pipe(livereload());
});

gulp.task('fonts', function() {
	return gulp.src(config.fontDir + '/**.*')
		.pipe(gulp.dest('./public/fonts'));
});

gulp.task('images', function() {
	return gulp.src(config.imageDir + '/**.*')
		.pipe(gulp.dest('./public/img'));
});
gulp.task('screenshot', function() {
    return gulp.src(config.imageDir + '/screenshot/**.*')
        .pipe(gulp.dest('./public/img/screenshot'));
});
gulp.task('videos', function() {
    return gulp.src(config.videoDir + '/**.*')
        .pipe(gulp.dest('./public/video'));
});

gulp.task('styles', function () {
	return gulp.src(config.sassDir +'/app.scss')
		.pipe(sass())
		.on("error", notify.onError(function (error) {
			return "Error: " + error.message;
		}))
		.pipe(postcss(processors))
		.pipe(gulp.dest('./public/css'))
		.pipe(livereload());
});

gulp.task('player', function() {
	return gulp.src(config.sassDir + '/player.scss')
		.pipe(sass())
		.on("error", notify.onError(function (error) {
			return "Error: " + error.message;
		}))
		.pipe(postcss(processors))
		.pipe(gulp.dest('./public/css'))
		.pipe(livereload());
});

gulp.task('scripts', function(){
	gulp.src(config.jsPath + '/*.js')
		.on("error", notify.onError(function (error) {
			return "Error: " + error.message;
		}))
		.pipe(concat('core.js'))
		// .pipe(uglify())
		.pipe(gulp.dest('./public/js'))
		.pipe(livereload());
});

gulp.task('connect', function() {
	connect.server({
		base      : 'public',
		livereload: true
	});
});

gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('resources/**.php', ['files']);
	gulp.watch(config.jsPath + '/*.js', ['scripts']);
	gulp.watch(config.sassDir + '/*.scss', ['styles']);
});

gulp.task('dev', ['files', 'footer', 'images', 'fonts', 'screenshot', 'videos', 'scripts', 'styles', 'player', 'connect', 'watch']);
